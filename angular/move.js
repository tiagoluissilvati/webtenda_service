var fs = require('fs')

var oldPath = '../src/main/resources/static/'
var newPath = '../src/main/resources/templates/'

if (!fs.existsSync(newPath)) {
    fs.mkdirSync(newPath);
}

fs.rename(oldPath + '/index.html', newPath + 'index.html', function (err) {
    if (err) throw err;
    console.log("index.html movido para resources/templates/")
});