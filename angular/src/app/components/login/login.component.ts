import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { AppoioLabService } from 'src/app/services/appoiolab.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private appoioLabService: AppoioLabService,
    private formBuilder: FormBuilder,private activatedRoute: ActivatedRoute) {

  }

  loginForm: any;
  submitted = false;

  async ngOnInit() {


    this.loginForm = this.formBuilder.group({
      nome: ['', Validators.required],
      senha: ['', Validators.required]
    });


  }
  get f() { return this.loginForm.controls; }
  onSubmit() {

    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    let formObj = this.loginForm.getRawValue();
 
    this.appoioLabService.login(formObj);
  }
}
