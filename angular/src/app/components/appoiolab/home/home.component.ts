import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {WebcamImage, WebcamInitError, WebcamUtil} from 'ngx-webcam';
import { AppoioLabService } from 'src/app/services/appoiolab.service';
import { MatDialog } from "@angular/material/dialog";
import { WebcamComponent } from '../comuns/webcan/webcam.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private appoioLabService  : AppoioLabService, public dialog: MatDialog) { }
  blockedPanel = false;
  WIDTH = 640;
  HEIGHT = 480;
  display  =   false;
  exibeChart = false;
  listaOS  = []  as  any;
  
  captures: string[] = [];
  error: any;
  isCaptured: boolean = false;


  basicData = {
    labels: [] as any,
    datasets: [
        {
            label: '',
            backgroundColor: '',
            data: [0]
        }
    ]
};

basicOptions: any;

  @ViewChild("video")
  public video!: ElementRef;

  @ViewChild("canvas")
  public canvas!: ElementRef;



  ngOnInit(): void {  
    
  }
  async  ngAfterViewInit() { 

    this.carregar();
    this.timeout();
         
  }
 
timeout() {
    setTimeout( () => { 
         
        this.carregar();
        this.timeout();
    }, 300000);
}

carregar() {
        this.blockedPanel = true;
        this.appoioLabService.getOS().subscribe( resp => { console.log(resp); this.listaOS = resp , this.initChart()});
}

initChart() {
    
   this.basicOptions = {
    plugins: {
        legend: {
            labels: {
                color: '#2850ff'
            }
        }
    },
    scales: { 
        x: {
            ticks: {
                color: '#2850ff'
            },
            grid: {
                color: '#ebedef',
                offset: false
            }
        },
        yAxes: [{
            ticks: {
                beginAtZero: true,
                callback: (value: number) => { if (value != null &&value % 1 === 0) {return value;} else {return}}

            } 
        }]
    }
  };

    this.listaOS.forEach((e: any , i: any ) =>     this.basicData.labels.push( e.hora) );
    this.basicData.labels =  [...new Set(this.basicData.labels)];

    this.basicData.datasets =  [
      {
          label: 'Atendimentos Agendados',
          backgroundColor: '#2850ff',
          data: []
      }
   ]
   
    this.basicData.labels.forEach((hora: any , i: any ) =>  {
        let ctn = 0; 
        this.listaOS.forEach((os: any , i: any ) =>   {
          if (hora == os.hora) {
            ctn++;
            
            this.exibeChart  = true;
          }

        } );
        this.basicData.datasets[0].data.push(ctn);
    } );

    console.log(this.basicOptions);

    this.blockedPanel = false;
  }

  foto(id: any) {
 
   const dialogRef = this.dialog.open(WebcamComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);

      this.appoioLabService.atualizar(id, null, result).subscribe( resp => { 
           this.dialog.closeAll();
           this.carregar();
      });


    });  
 
  }

 

  atualizarStatus(id: number, status: string) {
    this.blockedPanel = true;
    this.appoioLabService.atualizar(id, status, null).subscribe( resp => { 
      this.appoioLabService.getOS().subscribe( resp => { console.log(resp); this.listaOS = resp , this.initChart()});
     
    });
  }
 
}
