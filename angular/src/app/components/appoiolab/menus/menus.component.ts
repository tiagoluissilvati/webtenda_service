import { Component, HostBinding, Input, OnInit } from '@angular/core'; 
import { Router } from '@angular/router'; 
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NavItem } from 'src/app/models/nav-item';
import { NavService } from 'src/app/services/nav.service';
import { AppoioLabService } from 'src/app/services/appoiolab.service';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css'],
  animations: [
      trigger('indicatorRotate', [
          state('collapsed', style({ transform: 'rotate(0deg)' })),
          state('expanded', style({ transform: 'rotate(180deg)' })),
          transition('expanded <=> collapsed',
              animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
          ),
      ])
  ]
})
export class MenusComponent implements OnInit {

  expanded: boolean = false;

  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  @Input() item: any;
  @Input() depth: number = 0;

  constructor(public navService: NavService,
      public router: Router,
      private appoioLabService: AppoioLabService) {
           
      if (this.depth === undefined) {
          this.depth = 0;
      }
  }

  ngOnInit() {  
      this.navService.currentUrl.subscribe((url: string) => {
          if (this.item.route && url) {
              this.expanded = url.indexOf(`/${this.item.route}`) === 0;
              this.ariaExpanded = this.expanded;
          }
      });
  }

  onItemSelected(item: NavItem) {
      if (!item.children || !item.children.length) {
          this.appoioLabService.menuOpenEmmiter.emit(false);
          this.router.navigate([item.route]);
      }

      if (item.children && item.children.length) {
          this.expanded = !this.expanded;
      }
  }

}
