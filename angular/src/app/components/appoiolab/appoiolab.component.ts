import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NavItem } from 'src/app/models/nav-item';
import { AppoioLabService } from 'src/app/services/appoiolab.service';

@Component({
  selector: 'app-appoiolab',
  templateUrl: './appoiolab.component.html',
  styleUrls: ['./appoiolab.component.css']
})
export class WmsComponent implements OnInit { 
  @ViewChild('sidenav') sidenav!: MatSidenav;
  constructor(
    private router: Router,
    private appoioLabService: AppoioLabService) { }

  loginAuthSubscription !: Subscription ;
  menuOpenSubscription !: Subscription ;
  title = 'wms';
  opened: boolean = true;
  valueIconLeft: any;

  menu: NavItem[] | undefined ;

   
  async ngOnInit()  { 
   /* if (! this.menu) {
      this.appoioLabService.getUserMenus(); 
    }
    
    this.loginAuthSubscription = this.appoioLabService.loginMenusEmmiter.subscribe(l => { 
      this.menu = l; 
    });*/
    this.menuOpenSubscription = this.appoioLabService.menuOpenEmmiter.subscribe(l => {   
      this.sidenav.toggle();
    });

    
  }
  
  ngOnDestroy() {
    console.log("DESTROI")
    this.loginAuthSubscription.unsubscribe();
    this.menuOpenSubscription.unsubscribe();
  }

  logoff() {
    this.appoioLabService.logoff();
  }
   
}
