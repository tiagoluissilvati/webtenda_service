import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from 'src/app/guards/login.gard';
import { HomeComponent } from './home/home.component';
 
 

const routes: Routes = [
  {path: "", component: HomeComponent  ,  canActivate : [LoginGuard] },
  {path: "home", component: HomeComponent ,  canActivate : [LoginGuard]}, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EStoreRoutingModule { }
