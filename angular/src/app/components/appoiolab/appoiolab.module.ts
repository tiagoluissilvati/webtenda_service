import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';  

import { SharedModule } from 'src/app/shared/shared.module';
import { EStoreRoutingModule } from './appoiolab-routing.module';
import { HomeComponent } from './home/home.component';

    
 

@NgModule({
  declarations: [ 
    HomeComponent
  ],
  imports: [  
    SharedModule,
    EStoreRoutingModule
  ],
  exports:[]
})
export class EStoreModule { }
