import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MessageDTO } from 'src/app/models/message';
import {Message, MessageService} from 'primeng/api';
import { MessageService as MsgService }  from 'src/app/services/message.service';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css'], 
  providers: [MessageService]
})
export class MessagesComponent implements OnInit {
 

  private messagesSubscription!: Subscription;
  private clearSubscription!: Subscription;

  constructor(private msgService: MsgService, private primeMsgSevice: MessageService) { }

  ngOnInit() {
    this.messagesSubscription = this.msgService.messageEmmiter.subscribe(l => {  
      if (l && l.danger) { 
        for (let msg of l.danger) { 
          this.primeMsgSevice.add({severity:'error', summary:'Error', detail: msg})
         }; 
      }  
      if (l && l.info) { 
        for (let msg of l.info) { 
          this.primeMsgSevice.add({severity:'info', summary:'Info', detail:msg})
        };  
      }
      if (l && l.success) {  
        for (let msg of l.success) { 
            this.primeMsgSevice.add({severity:'success', summary:'Success', detail:msg})
         };  
      }
      
      if (l && l.warning) {   
        for (let msg of l.warning) { 
          this.primeMsgSevice.add({severity:'warn', summary:'Warning', detail:msg}) ;
        }; 
      } 

    });

    this.clearSubscription = this.msgService.clearEmmiter.subscribe(l => {  
      this.primeMsgSevice.clear();
    });
  }

  ngOnDestroy() {
    this.messagesSubscription.unsubscribe();
    this.clearSubscription.unsubscribe();
  }
}
