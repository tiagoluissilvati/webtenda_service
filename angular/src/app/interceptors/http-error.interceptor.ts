import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { MessageService } from "../services/message.service";
import { AppoioLabService } from "../services/appoiolab.service";

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor( private messageService: MessageService){ }
   
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(req).pipe(
          map((event: HttpEvent<any>) => {
              if (event instanceof HttpResponse) {
                
            if (event.body && event.body.mensagens) {
                this.messageService.clear();
                this.messageService.messageEmmiter.emit(event.body.mensagens);
             }   
              }
              return event;
          }), 
          
          catchError(
            (error: HttpErrorResponse) => { 
              return throwError(error);
            }
          )
      )
  }
   
}