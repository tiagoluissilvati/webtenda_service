import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';  
import { DeliveryReportComponent } from './components/delivery-report/delivery-report.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  {path: "", loadChildren: () => import('./components/appoiolab/appoiolab.module').then(m => m.EStoreModule) },
  {path: "delivery",  component: DeliveryReportComponent } ,   
  {path: "login",  component: LoginComponent } ,   
  {path: "**" , redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
