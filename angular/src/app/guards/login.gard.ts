import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { AppoioLabService } from "../services/appoiolab.service";


@Injectable({
    providedIn: 'root'
})
export class LoginGuard implements CanActivate {

    constructor(private route : Router, private appoioLabService :  AppoioLabService) {
    
    }

    canActivate(route:  ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean {
        var logged =  this.appoioLabService.isLogged(); 
        if (logged) {
            return true; 
        }  
        this.appoioLabService.isUserLogged = false; 
        this.appoioLabService.loginEmmiter.emit(false);
        this.route.navigate(['login']);
        return false;
    }
} 