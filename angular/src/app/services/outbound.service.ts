import { HttpClient } from "@angular/common/http";
import { Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router"; 
import { environment } from "src/environments/environment";
import { CostumerDeliveryFilter } from "../models/costumer-delivery-filter";

@Injectable({
    providedIn: 'root'
})
export class OutBoundService implements OnInit{
    
    

    constructor(private router: Router, private http : HttpClient ) { 
    }

    ngOnInit() {
    }

    listCostumerDelivery(filter: CostumerDeliveryFilter){
       
        return  this.http.post<any>(environment.urlAPI + "/outbound/costumer-delivery", filter); 
    }

    listDeliveryMatch(filter: any) {
      return  this.http.post<any>(environment.urlAPI + "/outbound/delivery-match", filter); 
    }

    invoiceInquiry(filter: any) {
        return  this.http.post<any>(environment.urlAPI + "/outbound/invoice-inquiry", filter); 
    }
    
    getPrinters() {
        return  this.http.get<any>(environment.urlAPI + "/outbound/printers"); 
    } 
    
    printLabel(invoice: any) {
        return  this.http.post<any>(environment.urlAPI + "/outbound/print-label", invoice); 
    }
    
}