import { Injectable, EventEmitter, OnInit } from "@angular/core";  
import { MessageDTO } from "../models/message"; 

@Injectable({
    providedIn: 'root'
})
export class MessageService implements OnInit {

    public messageEmmiter = new EventEmitter<MessageDTO>();
    public clearEmmiter = new EventEmitter<void>();
    
    constructor() { 
    }
    ngOnInit(): void { 
    }
    
    clear() {
        this.clearEmmiter.emit();
    }
    
}