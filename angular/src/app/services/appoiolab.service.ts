import { HttpClient } from "@angular/common/http";
import { Injectable, EventEmitter, OnInit } from "@angular/core"; 
import { Router } from "@angular/router";
import * as CryptoJS from 'crypto-js';   
import { environment } from "src/environments/environment";
import { NavItem } from "../models/nav-item";

@Injectable({
    providedIn: 'root'
})
export class AppoioLabService implements OnInit{

    constructor(private router: Router, private http : HttpClient ) {
 
    }
    ngOnInit() {
    }

    public isUserLogged: any ;

    public loginMenusEmmiter = new EventEmitter<NavItem[]>();
    public loginEmmiter = new EventEmitter<boolean>();
    public menuOpenEmmiter = new EventEmitter<boolean>();

    public  checkToken() {
      return  this.http.get<any>(environment.urlAPI + "/checktoken")
    }
    
    getUserMenus() {
        return  this.http.get<any>(environment.urlAPI + "/user-menus").subscribe( resp => { 
            this.loginMenusEmmiter.emit(resp.menus); 
        });
    }
    getOS() {
        
        let idTenda = localStorage.getItem('idTenda');
        return  this.http.get<any>(environment.urlAPI + "/ordemservico/" + idTenda)
    }
    
    public  login(login: any) {
        this.http.post<any>(environment.urlAPI + "/portal/login", login).subscribe( resp => {
            if (resp.success) {

                console.log(resp);
                localStorage.setItem('idTenda', resp.idTenda);
                this.setUserToken(resp.token)
                this.isUserLogged = true;  
                this.loginEmmiter.emit(true);
                this.loginMenusEmmiter.emit(resp.menus);
                this.router.navigate(['home']);
            }
        });
    }
    
    atualizar(id: number , status: any, foto : any) {
        let obj;
        if (status)
           obj = {idOS  :  id , status : status};
        else 
           obj = {idOS  :  id , foto : foto};

        return this.http.post<any>(environment.urlAPI + "/ordemservico/atualiza", obj);
    }

    public setUserToken(token : any) {
        localStorage.setItem('userToken',  token); 
    }
  

    isLogged() {        
        return  this.isUserLogged;
    }

    logoff() {
      this.isUserLogged = false;
      this.loginEmmiter.emit(false);
      
      localStorage.setItem('userName', '');
      localStorage.setItem('userToken', '');
      this.router.navigate(['login']);
    }

}
