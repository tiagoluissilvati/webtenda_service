import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';  
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component'; 

import { WmsComponent } from './components/appoiolab/appoiolab.component'; 
import { CommonModule, DatePipe } from '@angular/common';
import { MenusComponent } from './components/appoiolab/menus/menus.component';
import { LoginComponent } from './components/login/login.component';
import { SharedModule } from './shared/shared.module'; 
 import { DeliveryReportComponent } from './components/delivery-report/delivery-report.component';
import { EStoreModule } from './components/appoiolab/appoiolab.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from './interceptors/http-error.interceptor'; 
import { BrowserModule } from '@angular/platform-browser'; 
 
@NgModule({
  declarations: [
    AppComponent,  
    WmsComponent,  
    LoginComponent,
    MenusComponent,
    DeliveryReportComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    SharedModule,
    EStoreModule,
    AppRoutingModule
  ], 
  providers: [
    {
      provide : HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
