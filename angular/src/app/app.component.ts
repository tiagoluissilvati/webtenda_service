import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NavItem } from './models/nav-item';
import { AppoioLabService } from './services/appoiolab.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  isLogged = false;
  isReport= false;
  private loginSubscription!: Subscription;
  constructor(private appoioLabService: AppoioLabService, private router: Router, private activatedRoute: ActivatedRoute) {
    
    this.router.events.subscribe(
      (event: any) => {
        if (event instanceof NavigationEnd) {
          if (this.router.url === "/delivery") {
             this.isReport = true;
          } else {
             this.isReport = false;
          }
        }
      }
    );
  }

  async ngOnInit() {
    this.loginSubscription = this.appoioLabService.loginEmmiter.subscribe(l => { this.isLogged = l });
  }

  ngOnDestroy() {
    this.loginSubscription.unsubscribe();
  }
}
