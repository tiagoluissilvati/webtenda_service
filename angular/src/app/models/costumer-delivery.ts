export interface CostumerDelivery {
    id?: number;
    armazem?: string;
    planta?: string;
    nfNo?: string;
    deliveryNo?: string;
    serie?: string;
    cliente?: string;
    endereco?: string;
    complemento?: string;
    cidade?: string;
    estado?: string;
}
  