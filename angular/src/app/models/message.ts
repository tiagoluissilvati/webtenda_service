export class MessageDTO {

    danger: string[] | undefined;
    warning: string[] | undefined;
    success: string[] | undefined;
    info: string[] | undefined;

}