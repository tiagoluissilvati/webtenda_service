export class CostumerDeliveryFilter {
    deliveryNo?: string;
    nfNo?: string;
    planta?: string;
    armazem? : string;
    dtDelivery?: string;  
    firstResult? : number = 0;
    maxResult? : number = 5;
}
  