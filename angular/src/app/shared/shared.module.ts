import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";  
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { MatMenuModule } from "@angular/material/menu";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatToolbarModule } from "@angular/material/toolbar";

import { AutoCompleteModule } from "primeng/autocomplete";
import { CardModule } from "primeng/card";
import {DropdownModule} from 'primeng/dropdown';
import {InputTextModule} from 'primeng/inputtext';
import {CalendarModule} from 'primeng/calendar';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {DialogModule, Dialog} from 'primeng/dialog';
import { ChartModule } from 'primeng/chart';
import { WebcamModule} from 'ngx-webcam';
import {MatDialogModule} from '@angular/material/dialog';
import {BlockUIModule} from 'primeng/blockui';

import { MessagesComponent } from "../components/appoiolab/comuns/messages/messages.component";
import { BrowserModule } from "@angular/platform-browser";
import { WebcamComponent } from "../components/appoiolab/comuns/webcan/webcam.component";
 
@NgModule({
  declarations: [
    MessagesComponent,
    WebcamComponent
  ],
  imports: [  
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AutoCompleteModule,
    HttpClientModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatMenuModule, 
    MatButtonModule ,
    MatCardModule   ,
    CardModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    ButtonModule,
    TableModule,
    MessageModule,
    MessagesModule,
    InputTextareaModule,
    DialogModule ,
    ChartModule ,
    WebcamModule,
    MatDialogModule,
    BlockUIModule
   ],
  exports: [ 
    CommonModule,
    FormsModule,
    ReactiveFormsModule, 
    AutoCompleteModule,
    HttpClientModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatMenuModule, 
    MatButtonModule ,
    MatCardModule   ,
    CardModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    ButtonModule,
    TableModule,
    MessageModule,
    MessagesModule,
    MessagesComponent,
    InputTextareaModule,
    DialogModule ,
    ChartModule ,
    WebcamModule,
    MatDialogModule,
    BlockUIModule
  ]
})
export class SharedModule {}