package br.com.tls.appoiolab.controller.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.tls.appoiolab.component.OrdemServicoComponent;
import br.com.tls.appoiolab.dto.OutDTO;
import br.com.tls.appoiolab.dto.api.OrdenServicoDTO;
import br.com.tls.appoiolab.dto.api.TendaDTO;
import br.com.tls.appoiolab.utils.RequestUtils;

@RestController
public class OrdenServicoRestController {

	private @Autowired HttpServletRequest request;
	@Autowired
	private OrdemServicoComponent ordenServicoComponent; 
 
	@PostMapping(value= "/consultar/ordemservico", produces = "application/json")
	public @ResponseBody List<OrdenServicoDTO> getOs(@RequestBody  OrdenServicoDTO os) throws Exception {
 
		return ordenServicoComponent.getOSs(os.getIdCliente());
	}
  
	@PostMapping(value= "/ordemservico", produces = "application/json")
	public @ResponseBody OutDTO gerarOS(@RequestBody  OrdenServicoDTO os) throws Exception {
		ordenServicoComponent.gerarOS(os); 
		return new OutDTO();
	}
	@GetMapping(value= "/ordemservico/{idTenda}", produces = "application/json")
	public @ResponseBody  List<OrdenServicoDTO>  gerarOS(@PathVariable("idTenda") Long idTenda) throws Exception {		
		return ordenServicoComponent.getOsPorTendaHoje(idTenda);
	}
  
	@GetMapping(value= "/tendas" , produces = "application/json")
	public List<TendaDTO> getTendas(){  
		return ordenServicoComponent.getTendas();
	}

	@PostMapping(value= "/ordemservico/atualiza" , produces = "application/json")
	public OutDTO updateStatus(@RequestBody  OrdenServicoDTO os) {
		return ordenServicoComponent.updateStatus(os);
	}
	
	
	
	
}
