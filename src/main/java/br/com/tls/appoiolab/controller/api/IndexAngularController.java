package br.com.tls.appoiolab.controller.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexAngularController {

	@GetMapping(value = {"/", "/**/{path:[^.]*}"})
	public String index() {
		
		return "index";
	}
}
