package br.com.tls.appoiolab.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.tls.appoiolab.component.UsuarioComponent;
import br.com.tls.appoiolab.dto.api.UsuarioDTO;
import br.com.tls.appoiolab.entity.AcessoPortal;
import br.com.tls.appoiolab.exception.ValidacaoException;

@RestController
public class UsuarioRestController {
	
	@Autowired
	private UsuarioComponent usuarioComponent;

	@PostMapping(value = "/usuario", produces = "application/json")
	public @ResponseBody UsuarioDTO salvar(@RequestBody UsuarioDTO usuDTO) throws ValidacaoException {
		
		return usuarioComponent.salvar(usuDTO);
	}

	@PostMapping(value = "/consulta/usuario", produces = "application/json")
	public @ResponseBody UsuarioDTO findByEmail(@RequestBody  UsuarioDTO usuDTO) throws ValidacaoException {
		
		return usuarioComponent.findByEmail(usuDTO.getEmail());
	}

	@PostMapping(value = "/login", produces = "application/json")
	public @ResponseBody UsuarioDTO login(@RequestBody  UsuarioDTO usuDTO) throws ValidacaoException {
 
		return usuarioComponent.login(usuDTO);
	}

	@PostMapping(value = "/recuperar", produces = "application/json")
	public @ResponseBody UsuarioDTO recuperar(@RequestBody  UsuarioDTO usuDTO) throws ValidacaoException {
		
		return usuarioComponent.recuperarSenha(usuDTO);
	}
	


	@PostMapping(value = "/portal/login", produces = "application/json")
	public @ResponseBody UsuarioDTO loginPortal(@RequestBody  UsuarioDTO usuDTO) throws ValidacaoException {
		AcessoPortal ac = usuarioComponent.loginPortal(usuDTO);
		UsuarioDTO usu = new UsuarioDTO();
		usu.setIdTenda(ac.getIdTenda());
		return usu;
	}
	
}
