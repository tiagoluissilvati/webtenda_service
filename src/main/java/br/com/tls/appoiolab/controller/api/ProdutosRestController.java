package br.com.tls.appoiolab.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.tls.appoiolab.component.ProdutoComponent;
import br.com.tls.appoiolab.dto.api.ProdutoDTO;
import br.com.tls.appoiolab.exception.ValidacaoException;

@RestController
public class ProdutosRestController {
 
	@Autowired
	private ProdutoComponent produtoComponent;

	@GetMapping(value =  "/produtos", produces = "application/json")
	public @ResponseBody List<ProdutoDTO> salvar() throws ValidacaoException {
		
		return produtoComponent.findAll();
	}
	 
}
