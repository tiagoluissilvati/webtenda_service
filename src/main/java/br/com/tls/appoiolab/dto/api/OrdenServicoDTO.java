package br.com.tls.appoiolab.dto.api;

import lombok.Data;

@Data
public class OrdenServicoDTO {

	private String idCliente;
	private String tendaId;
	private String tendaDs;
	private String data;
	private String hora;
	private String pcr;
	private String antigeno;
	
	private String url;

	private String idOS;
	private String nome;
	private String status;
	
	private String foto;
	
}
