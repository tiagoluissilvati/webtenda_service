package br.com.tls.appoiolab.dto.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TendaDTO {

	private Long idTenda;
	private String descricao;
	
}
