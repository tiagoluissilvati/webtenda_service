package br.com.tls.appoiolab.dto.api;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.tls.appoiolab.dto.OutDTO;
import lombok.Data;

@Data
public class UsuarioDTO extends OutDTO {
	 
	  private Long idUsuario; 
	  private String nome;
 
	  private String documento;
 
	  private String sexo;

	  @Temporal(TemporalType.DATE)
	  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", locale = "pt-BR", timezone = "Brazil/East")
	  private Date dtnac;
 
	  private String celular;
 
	  private String email;
	   
	  private String senha;
	   
	  private String tpUsuario;
	  
	  private Long idTenda;
}
