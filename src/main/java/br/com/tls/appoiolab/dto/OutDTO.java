package br.com.tls.appoiolab.dto;

import lombok.Data;

@Data
public class OutDTO {
 
	private boolean success = true;
	private String msg;
	private MensagensDTO mensagens;
}
