package br.com.tls.appoiolab.dto.api;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ProdutoDTO   {

	  private Long idProduto;
	    
	  private String descricao;
	   
	  private BigDecimal valor;
}
