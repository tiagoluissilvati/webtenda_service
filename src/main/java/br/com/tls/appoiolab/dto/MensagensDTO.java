package br.com.tls.appoiolab.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class MensagensDTO {

	private List<String> danger;
	private List<String> warning;
	private List<String> success;
	private List<String> info;
	
	public boolean hasMensagem() {
		return !(CollectionUtils.isEmpty(danger) && 
				CollectionUtils.isEmpty(info) &&
				CollectionUtils.isEmpty(success) &&
				CollectionUtils.isEmpty(warning));
	}
	

	public void addDanger(String msg) {
		if (danger == null) { 
			danger = new ArrayList<>();
		}
		danger.add(msg);
	}
	public void addWarnig(String msg) {
		if (warning == null) { 
			warning = new ArrayList<>();
		}
		warning.add(msg);
	}
	public void addSuccess(String msg) {
		if (success == null) { 
			success = new ArrayList<>();
		}
		success.add(msg);
	}
	public void addInfo(String msg) {
		if (info == null) { 
			info = new ArrayList<>();
		}
		info.add(msg);
	}
	
	
}
