package br.com.tls.appoiolab.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Table(name = "ordemservico")
@Data
public class OrdenServico {

    @Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idordemservico")
	private Long idOrdemServico;
    
    @Column(name="idusuario")
	private Long idUsuario;
    @Column(name="idtenda")
	private Long idTenda;
    @Column(name="idproduto")
	private Long idProduto;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="data") 
	private Date data;
    @Column(name="status")
	private String status;
    @Column(name="retorno")
	private String retorno;

    @Column(name="foto")
    @Lob
    private String foto;
    
	
}
