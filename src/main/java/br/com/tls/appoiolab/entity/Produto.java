package br.com.tls.appoiolab.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "produto")
@Data
public class Produto {

	  @Id 
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @Column(name="idproduto")
	  private Long idProduto;
	   
	  @Column(name="descricao")
	  private String descricao;
	  
	  @Column(nullable= false, precision=15, scale=2) 
	  private BigDecimal valor;
	  
	  
}
