package br.com.tls.appoiolab.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "tenda")
@Data
public class Tenda {


	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @Column(name="idtenda")
	  private Long idTenda;
	  
	  private String descricao;
}
