package br.com.tls.appoiolab.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Table(name = "usuario")
@Data
public class Usuario {

	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @Column(name="idusuario")
	  private Long idUsuario;

	  @Column(name="nome")
	  private String nome;

	  @Column(name="documento")
	  private String documento;

	  @Column(name="sexo")
	  private String sexo;

	  @Temporal(TemporalType.DATE)
	  @Column(name="dtnac")
	  private Date dtnac;

	  @Column(name="celular")
	  private String celular;

	  @Column(name="email")
	  private String email;
	  
	  @Column(name="senha")
	  private String senha;
	  
	  @Column(name="tp_usuario")
	  private String tpUsuario;
	  
}
