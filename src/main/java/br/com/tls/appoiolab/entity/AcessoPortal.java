package br.com.tls.appoiolab.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "acessoportal")
@Data
public class AcessoPortal {

	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @Column(name="idacessoportal")
	  private Long idAcessoPortal;

	  @Column(name="idtenda")
	  private Long idTenda;

	  @Column(name="senha")
	  private String senha;
	  
	  @Column(name="login")
	  private String login;
}
