package br.com.tls.appoiolab.soap.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Solicitacoes {
 
    @XmlAttribute(name="versao")
	public String versao = "20070110";

    @XmlElement(name="entidade")
 	public Entidade entidade;

	public void setVersao(String versao) {
		this.versao = versao;
	}
 
	public void setEntidade(Entidade entidade) {
		this.entidade = entidade;
	}
 
	public String getVersao() {
		return versao;
	}
 
	public Entidade getEntidade() {
		return entidade;
	}  
}
