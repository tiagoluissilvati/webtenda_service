package br.com.tls.appoiolab.soap.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Solicitacao {

    @XmlAttribute(name="codigo_lis")
	public String codigo_lis;
    @XmlAttribute(name="codigo_paciente")
	public String codigo_paciente;
    @XmlAttribute(name="crm")
	public String crm;
    @XmlAttribute(name="data")
	public String data;
    @XmlElement(name="amostra")
	public Amostra amostra;
	public String getCodigo_lis() {
		return codigo_lis;
	}
	public void setCodigo_lis(String codigo_lis) {
		this.codigo_lis = codigo_lis;
	}
	public String getCodigo_paciente() {
		return codigo_paciente;
	}
	public void setCodigo_paciente(String codigo_paciente) {
		this.codigo_paciente = codigo_paciente;
	}
	public String getCrm() {
		return crm;
	}
	public void setCrm(String crm) {
		this.crm = crm;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Amostra getAmostra() {
		return amostra;
	}
	public void setAmostra(Amostra amostra) {
		this.amostra = amostra;
	}
  
}
