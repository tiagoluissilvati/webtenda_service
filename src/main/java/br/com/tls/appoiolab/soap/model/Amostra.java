package br.com.tls.appoiolab.soap.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Amostra {

    @XmlAttribute(name="material")
	private String material;
    @XmlElement(name="exame")
	private Exame exame;
 
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	} 
	public Exame getExame() {
		return exame;
	}
	public void setExame(Exame exame) {
		this.exame = exame;
	}
	
 
}
