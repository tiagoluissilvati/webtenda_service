package br.com.tls.appoiolab.soap.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Paciente {

    @XmlAttribute(name="codigo_lis")
	public String codigo_lis;
    @XmlAttribute(name="nome")
	public String nome;
    @XmlAttribute(name="datanasc")
	public String datanasc;
    @XmlAttribute(name="sexo")
	public String sexo;
	 
	public String getCodigo_lis() {
		return codigo_lis;
	}
	public void setCodigo_lis(String codigo_lis) {
		this.codigo_lis = codigo_lis;
	} 
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	} 
	
	public String getDatanasc() {
		return datanasc;
	}
	
	public void setDatanasc(String datanasc) {
		this.datanasc = datanasc;
	} 
	
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	 
	
}
