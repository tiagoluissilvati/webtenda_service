package br.com.tls.appoiolab.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.tls.appoiolab.entity.OrdenServico;

@Repository
public interface OrdemServicoRepository  extends JpaRepository<OrdenServico, Long>{

	List<OrdenServico> findByIdUsuario(Long idUsuario);

	List<OrdenServico> findByIdTendaAndDataGreaterThanEqualAndDataLessThanEqual(Long idTenda, Date inicio, Date dtFinal);

	List<OrdenServico> findByIdTendaAndDataGreaterThanEqualAndDataLessThanEqualAndStatus(Long idTenda, Date time,
			Date time2, String status);
  
}
