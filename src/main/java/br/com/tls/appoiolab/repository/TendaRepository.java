package br.com.tls.appoiolab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.tls.appoiolab.entity.Tenda;

@Repository
public interface TendaRepository  extends JpaRepository<Tenda, Long>{
 
	
}
