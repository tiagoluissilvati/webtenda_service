package br.com.tls.appoiolab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.tls.appoiolab.entity.Usuario;

@Repository
public interface UsuarioRepository  extends JpaRepository<Usuario, Long>{

	public Usuario findByEmail(String email);

	public Usuario findByDocumento(String documento);
	
}
