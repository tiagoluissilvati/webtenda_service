package br.com.tls.appoiolab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.tls.appoiolab.entity.AcessoPortal;

@Repository
public interface AcessoPortalRepository extends JpaRepository<AcessoPortal, Long>{

	AcessoPortal findByLoginAndSenha(String login, String senha);

}
