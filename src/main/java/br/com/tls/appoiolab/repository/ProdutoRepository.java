package br.com.tls.appoiolab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.tls.appoiolab.entity.Produto;

@Repository
public interface ProdutoRepository  extends JpaRepository<Produto, Long>{

	public Produto findByDescricao(String string);
 
	
}
