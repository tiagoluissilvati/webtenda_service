package br.com.tls.appoiolab.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import br.com.tls.appoiolab.dto.api.UsuarioDTO;
import br.com.tls.appoiolab.email.EmailService;
import br.com.tls.appoiolab.entity.AcessoPortal;
import br.com.tls.appoiolab.entity.Usuario;
import br.com.tls.appoiolab.exception.ValidacaoException;
import br.com.tls.appoiolab.repository.AcessoPortalRepository;
import br.com.tls.appoiolab.repository.TendaRepository;
import br.com.tls.appoiolab.repository.UsuarioRepository;

@Component
public class UsuarioComponent {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private AcessoPortalRepository acessoPortalRepository;

	@Autowired
	private TendaRepository tendaRepository;

	
	@Autowired
	private EmailService emailService;
	
	/**
	 * Cosulta usuario por email
	 * @param email
	 * @return
	 * @throws ValidacaoException
	 */
	public UsuarioDTO findByEmail(String email ) throws ValidacaoException {
		Usuario usu = usuarioRepository.findByEmail(email);
		validarUsuario(usu);
		return usuarioTOUsuarioDTO(usuarioRepository.findByEmail(email), new UsuarioDTO()) ;
	}
	
	  

	/**
	 * Cria e Atualiza dados do usuario
	 * @param usuDTO
	 * @return
	 * @throws ValidacaoException
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
	public UsuarioDTO salvar(UsuarioDTO usuDTO) throws ValidacaoException {
		Usuario usu = null;
		// NOVO USUARIO
		if (usuDTO.getIdUsuario() == null) {
			
			usu = usuDtoUsuario(new Usuario(), usuDTO);
			
			if (usuDTO.getDocumento() == null || usuDTO.getDocumento().trim().length() == 0) {
				throw new ValidacaoException("O campo Documento deve ser informado");
			} else {
				Usuario usuCPF = usuarioRepository.findByDocumento(usuDTO.getDocumento());
				if (usuCPF != null) { 
					throw new ValidacaoException("Usuário já Cadastrado ! Recupere a sua senha para realizar o Login");
				}
				Usuario usuEmail = usuarioRepository.findByEmail(usuDTO.getEmail());
				if (usuEmail != null) { 
					throw new ValidacaoException("E-mail já Cadastrado ! Recupere a sua senha para realizar o Login");
				}
			}
			 
			
		} else {
			usu = usuarioRepository.findById(usuDTO.getIdUsuario()).orElse(null);
			if (usu == null) {
				throw new ValidacaoException("Usuário não encontrado");
			} else {
				usu = usuDtoUsuario(usu, usuDTO); // Edita os dados do usuario
			}
		}
		validarUsuario(usu);
		usuarioRepository.save(usu);
		
		return usuarioTOUsuarioDTO(usu, usuDTO);
	}

	/**
	 * Converte um DTO para uma entidade Usuario
	 * @param usu
	 * @param usuDTO
	 * @return
	 */
	private Usuario usuDtoUsuario(Usuario usu, UsuarioDTO usuDTO) {
		if (usuDTO.getNome() != null && usuDTO.getNome().length() > 0) {
			usu.setNome(usuDTO.getNome().trim()); 
		}
		if (usuDTO.getDocumento() != null && usuDTO.getDocumento().length() > 0) {
			usu.setDocumento(usuDTO.getDocumento().trim());
		}
		if (usuDTO.getDtnac() != null ) {
			usu.setDtnac(usuDTO.getDtnac());
		}
		if (usuDTO.getCelular() != null && usuDTO.getCelular().length() > 0) {
			usu.setCelular(usuDTO.getCelular().trim());
		}
		if (usuDTO.getEmail() != null && usuDTO.getEmail().length() > 0) {
			usu.setEmail(usuDTO.getEmail().trim());
		}
		if (usuDTO.getSexo() != null && usuDTO.getSexo().length() > 0) {
			usu.setSexo(usuDTO.getSexo().substring(0, 1).toUpperCase().trim());
		}
		if (usuDTO.getSenha() != null && usuDTO.getSenha().length() > 0 ) {
			usu.setSenha(usuDTO.getSenha().trim());
		}
		usu.setTpUsuario("APP");
		return usu;
	} 
	
	/**
	 * Converte uma Entidade para o DTO 
	 * @param usu
	 * @param usuDTO
	 * @return
	 */
	private UsuarioDTO usuarioTOUsuarioDTO(Usuario usu, UsuarioDTO usuDTO) {
		usuDTO.setIdUsuario(usu.getIdUsuario());
		usuDTO.setNome(usu.getNome()); 
		usuDTO.setDocumento(usu.getDocumento());
		usuDTO.setDtnac(usu.getDtnac());
		usuDTO.setCelular(usu.getCelular());
		usuDTO.setEmail(usu.getEmail());
		return usuDTO;
	}



	public UsuarioDTO login(UsuarioDTO usuDTO) throws ValidacaoException {

		Usuario usu = usuarioRepository.findByEmail(usuDTO.getEmail());
		validarUsuario(usu);
		
		if (!usu.getSenha().trim().equals(usuDTO.getSenha().trim())) {
			throw new ValidacaoException("Usuário / senha inválidos");
		}
		
		return usuarioTOUsuarioDTO(usu, usuDTO);
	}
	
	private void validarUsuario(Usuario usu) throws ValidacaoException { 

		if (usu == null) {
			throw new ValidacaoException("Usuário não encontrado");
		}
		if (usu.getNome() == null || usu.getNome().trim().length() == 0) {
			throw new ValidacaoException("O campo Nome deve ser informado");
		}
		
		if (usu.getEmail() == null || usu.getEmail().trim().length() == 0) {
			throw new ValidacaoException("O campo Email deve ser informado");
		}
	}



	public UsuarioDTO recuperarSenha(UsuarioDTO usuDTO) {
		 
		 Usuario usu  = usuarioRepository.findByEmail(usuDTO.getEmail().trim());
		 if (usu == null) {
			 usu  = usuarioRepository.findByDocumento(usuDTO.getDocumento());
		 }
		 if (usu != null) {
			 final Usuario usuario = usu;
			 try {
				    new Thread(new Runnable() {
				        private EmailService myParam;

				        public Runnable init(EmailService myParam) {
				            this.myParam = myParam;
				            return this;
				        }

				        @Override
				        public void run() {
				      	  this.myParam.sendSimpleMessage(usuario.getEmail().trim(), "Recuperação Senha APPoioLab", "Sua senha é: " + usuario.getSenha());
							
				        }
				    }.init(this.emailService)).start();
			} catch (Exception e) { 
			}
		 }
		 
		 return new UsuarioDTO();
	}



	public AcessoPortal loginPortal(UsuarioDTO usuDTO) throws ValidacaoException {
	
		AcessoPortal a = acessoPortalRepository.findByLoginAndSenha(usuDTO.getNome(), usuDTO.getSenha());
	//	Tenda tenda = tendaRepository.getById(a.getIdTenda());
		if (a == null) { 
			throw new ValidacaoException("Usuário não encontrado");
		}
		AcessoPortal aa = new AcessoPortal();
		aa.setIdTenda(a.getIdTenda());
		return aa;
	}
}
