package br.com.tls.appoiolab.component;

import java.io.StringWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.xml.bind.JAXB;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.tls.appoiolab.dto.OutDTO;
import br.com.tls.appoiolab.dto.api.OrdenServicoDTO;
import br.com.tls.appoiolab.dto.api.ProdutoDTO;
import br.com.tls.appoiolab.dto.api.TendaDTO;
import br.com.tls.appoiolab.entity.OrdenServico;
import br.com.tls.appoiolab.entity.Produto;
import br.com.tls.appoiolab.entity.Tenda;
import br.com.tls.appoiolab.entity.Usuario;
import br.com.tls.appoiolab.repository.OrdemServicoRepository;
import br.com.tls.appoiolab.repository.ProdutoRepository;
import br.com.tls.appoiolab.repository.TendaRepository;
import br.com.tls.appoiolab.repository.UsuarioRepository;
import br.com.tls.appoiolab.soap.IAtriaLisWS;
import br.com.tls.appoiolab.soap.IAtriaLisWSservice;
import br.com.tls.appoiolab.soap.model.Amostra;
import br.com.tls.appoiolab.soap.model.Entidade;
import br.com.tls.appoiolab.soap.model.Exame;
import br.com.tls.appoiolab.soap.model.Paciente;
import br.com.tls.appoiolab.soap.model.Pacientes;
import br.com.tls.appoiolab.soap.model.Solicitacao;
import br.com.tls.appoiolab.soap.model.Solicitacoes;

@Component
public class OrdemServicoComponent {

	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private TendaRepository tendaRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private OrdemServicoRepository ordemServicoRepository;
	
	public List<ProdutoDTO> findAll(){
		
		List<Produto> prods = produtoRepository.findAll();
		
		List<ProdutoDTO> dtos = new ArrayList<>();
		for (Produto pro : prods) {
			dtos.add(produtoToDTO(pro));
		}
		
		return dtos;
	}
	
	public List<TendaDTO> getTendas(){
		List<TendaDTO> ret = new ArrayList<>();
		
		List<Tenda> tendas = tendaRepository.findAll();
		
		for (Tenda tenda : tendas) {
			ret.add(new TendaDTO(tenda.getIdTenda(), tenda.getDescricao().trim()));
		}
		ret.sort((a, b) -> a.getDescricao().compareTo(b.getDescricao()));
		return ret;
		
	}
	
	public ProdutoDTO produtoToDTO(Produto pro) {
		ProdutoDTO dto = new ProdutoDTO();
		
		dto.setIdProduto(pro.getIdProduto());
		dto.setDescricao(pro.getDescricao());
		dto.setValor(pro.getValor());
		return dto;
	}

	public OutDTO gerarOS(OrdenServicoDTO osDTO) throws Exception {
		
		
		if (osDTO.getPcr() != null && osDTO.getPcr().toUpperCase().equals("TRUE")) {
			
			Produto pro = produtoRepository.findByDescricao("RT-PCR");
			gerarOS(osDTO, pro);
		} 
		
		if (osDTO.getAntigeno() != null && osDTO.getAntigeno().toUpperCase().equals("TRUE")) { 
			Produto pro = produtoRepository.findByDescricao("ANTIGENO");
			gerarOS(osDTO, pro);
		}
		
		return new OutDTO();
	}
	
	private void transmitirOS(OrdenServico os ) {
		Solicitacoes sol = new Solicitacoes();
        Produto pro = produtoRepository.getById(os.getIdProduto());
		Usuario usu = usuarioRepository.getById(os.getIdUsuario());
		
		Entidade entidade = new Entidade();
		Pacientes pacientes = new Pacientes();
		Paciente paciente = new Paciente();
		paciente.setCodigo_lis(usu.getIdUsuario().toString());
		paciente.setNome(usu.getNome());
		paciente.setSexo(usu.getSexo());
		paciente.setDatanasc((new SimpleDateFormat("dd/MM/yyyy")).format(usu.getDtnac()));
		pacientes.setPaciente(paciente);

		entidade.setPacientes(pacientes);

		Solicitacao so = new Solicitacao();

		Amostra amo = new Amostra();
		Exame ex = new Exame();
		

		if (pro.getDescricao().equals("RT-PCR")) { 
			ex.setCodigo("CORN");
		} else { 
			ex.setCodigo("COV19A");
		}
		;
		amo.setExame(ex);
		amo.setMaterial("206");
		so.setAmostra(amo);
		so.setCodigo_lis(os.getIdOrdemServico().toString());
		so.setCodigo_paciente(usu.getIdUsuario().toString());
		so.setCrm("");
		so.setData((new SimpleDateFormat("dd/MM/yyyy")).format(os.getData()));
		entidade.setSolicitacao(so);

		sol.setEntidade(entidade);

		StringWriter sw = new StringWriter();
		JAXB.marshal(sol, sw);

		URL newWsdlLocation;
		try {
			newWsdlLocation = new URL(
					"http://api.avantix.com.br:8085/cgi-bin/AvantixWS/AtriaLisWS.exe/wsdl/IAtriaLisWS");

			IAtriaLisWSservice someService = new IAtriaLisWSservice(newWsdlLocation);

			IAtriaLisWS service = someService.getIAtriaLisWSPort();
			System.out.println(sw.toString());
			String ret = service.putInserirPedido(sw.toString());
			System.out.println(ret);
			os.setStatus("INTEGRADO");
			os.setRetorno(ret);
		} catch (Exception e) {

			os.setStatus("ERRO");
			e.printStackTrace();
		}

		
		ordemServicoRepository.save(os);
	}
	
	private void  gerarOS(OrdenServicoDTO osDTO, Produto pro) throws Exception {
		OrdenServico os = new OrdenServico();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	 
		os.setData(sdf.parse(osDTO.getData()+ " " + osDTO.getHora()));
		os.setIdTenda(Long.valueOf(osDTO.getTendaId()));
		os.setIdUsuario(Long.valueOf(osDTO.getIdCliente()));
		os.setIdProduto(pro.getIdProduto());
		os.setStatus("CRIADO");
		
		ordemServicoRepository.save(os);
		
	}

	public List<OrdenServicoDTO> getOSs(String idUsuario) {
		List<OrdenServico> oss = ordemServicoRepository.findByIdUsuario(Long.valueOf(idUsuario));
		List<OrdenServicoDTO> ret = getListDTO(oss);
		
		return ret;
	}

	private List<OrdenServicoDTO> getListDTO(List<OrdenServico> oss) {
		List<OrdenServicoDTO> ret = new ArrayList<OrdenServicoDTO>();
		for (OrdenServico ordenServico : oss) {
			OrdenServicoDTO osRet = new OrdenServicoDTO();
			osRet.setTendaDs(tendaRepository.getById(ordenServico.getIdTenda()).getDescricao().trim());
			Produto pro = produtoRepository.getById(ordenServico.getIdProduto());
			Usuario usu = usuarioRepository.getById(ordenServico.getIdUsuario());
			osRet.setPcr(pro.getDescricao());

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm");
			osRet.setData(sdf.format(ordenServico.getData()));
			osRet.setHora(sdfHora.format(ordenServico.getData()));
			osRet.setNome(usu.getNome());
			osRet.setIdOS(ordenServico.getIdOrdemServico().toString());
			osRet.setUrl("URL");
			if (ordenServico.getStatus().trim().equals("CONCLUIDA")) {
			}
			if (ordenServico.getFoto() !=  null) {
				System.out.println(ordenServico.getFoto());
			}
			ret.add(osRet);
		}
		return ret;
	}
	

	public List<OrdenServicoDTO> getOsPorTendaHoje(Long idTenda){
		Calendar dtInicio = Calendar.getInstance();
		dtInicio.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
		dtInicio.set(Calendar.HOUR_OF_DAY, 0);
		dtInicio.set(Calendar.MINUTE, 0);
		dtInicio.set(Calendar.SECOND, 0);
		dtInicio.set(Calendar.MILLISECOND, 0); 
		Calendar dtFinal = Calendar.getInstance();
		dtFinal.set(Calendar.HOUR_OF_DAY, 23);
		dtFinal.set(Calendar.MINUTE, 59);
		dtFinal.set(Calendar.SECOND, 59);
		dtFinal.set(Calendar.MILLISECOND, 0); 
		
		List<OrdenServico> oss = ordemServicoRepository.findByIdTendaAndDataGreaterThanEqualAndDataLessThanEqualAndStatus(idTenda, dtInicio.getTime(), dtFinal.getTime(), "CRIADO");
		List<OrdenServicoDTO> ret = getListDTO(oss);
		
		return ret; 
		
	}

	public OutDTO updateStatus(OrdenServicoDTO osDTO) {
		OrdenServico os = ordemServicoRepository.getById(new Long(osDTO.getIdOS()));
		if (os != null) {
			if (osDTO.getStatus() != null) {

				os.setStatus(osDTO.getStatus());	
				if ("APROVADO".equals(osDTO.getStatus())) {

					transmitirOS(os);
				}
			}
			
			if (osDTO.getFoto() != null) {
				os.setFoto(osDTO.getFoto());
			}
			ordemServicoRepository.save(os);
		}
		
		return new OutDTO();
	}
}
