package br.com.tls.appoiolab.component;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.tls.appoiolab.dto.api.ProdutoDTO;
import br.com.tls.appoiolab.entity.Produto;
import br.com.tls.appoiolab.repository.ProdutoRepository;

@Component
public class ProdutoComponent {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	
	public List<ProdutoDTO> findAll(){
		
		List<Produto> prods = produtoRepository.findAll();
		
		List<ProdutoDTO> dtos = new ArrayList<>();
		for (Produto pro : prods) {
			dtos.add(produtoToDTO(pro));
		}
		
		return dtos;
	}
	
	public ProdutoDTO produtoToDTO(Produto pro) {
		ProdutoDTO dto = new ProdutoDTO();
		
		dto.setIdProduto(pro.getIdProduto());
		dto.setDescricao(pro.getDescricao());
		dto.setValor(pro.getValor());
		return dto;
	}
	
}
