package br.com.tls.appoiolab.exception;

public class ValidacaoException  extends Exception{
 
	public ValidacaoException(String msg) {
		super(msg);
	}

	private static final long serialVersionUID = 1L;

}
