package br.com.tls.appoiolab.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.tls.appoiolab.dto.MensagensDTO;
import br.com.tls.appoiolab.dto.OutDTO;
import lombok.RequiredArgsConstructor;

@RestControllerAdvice
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ExceptionRestControllerAdvice {
	
	  @ExceptionHandler(ValidacaoException.class)
	  @ResponseStatus(value = HttpStatus.OK)
	  public OutDTO validacaoExceptionAdvice(ValidacaoException ex) {
 
	    OutDTO ret = new OutDTO();
	    MensagensDTO msgs   = new MensagensDTO();
	    msgs.addDanger(ex.getMessage());
	    ret.setSuccess(false);
	    ret.setMensagens(msgs);
	    return ret;
	  }
	
}
